from time import sleep
import bluedev as _BLUEDEV_
import mqtt as _MQTT_
import time as _TIME_

##############################################################################
# vaiables
timeout = 3.0
host = "192.168.0.163"
port = 1883
##############################################################################
# turn on bluetooth if it is not already on
if not _BLUEDEV_.isEnabled():
    _BLUEDEV_.turnOn()

# init MQTT
_MQTT_.init(host,port)
# start scanning for 1 hour
while True:
    _BLUEDEV_.startSCAN(timeout)
    _TIME_.sleep(timeout)
    print("Restart Bluetooth Advertisement")
