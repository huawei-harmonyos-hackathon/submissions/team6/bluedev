## -*- coding: utf-8 -*-
##############################################################################
# @file: bluedev.py
# @brief: implemantation for Standard BLE library for python
# @dependencies: pyBluez
# @version 1.0
# @author: Pratik Prajapati
##############################################################################
import os as _OS_
import time as _TIME_
import subprocess as _SUBPROCESS_
import socket as _SOCKET_
import struct as _STRUCT_
import sys as _SYS_
import bluetooth._bluetooth as _BLUETOOTH_
import mqtt as _MQTT_
from bluepy.btle import Scanner, DefaultDelegate, BTLEException
import requests as _REQUESTS_
import json as _JSON_
data = _JSON_.loads('{"temperature":0.0,"humidity":0.0,"iaq":0,"voc":0.0,"aqi":0,"rssi": -1}')
my_token_id = "7b7c27047351d47a2413256f2d69c3f38bbfee92"
my_city = "Frankfurt"
url = f"https://api.waqi.info/feed/{my_city}/?token={my_token_id}"
aqi = 0
##############################################################################
# VARIABLES
# BLE ADV
SCAN_ENABLE = 0x01
SCAN_DISABLE = 0x00
OGF_LE_CTL = 0x08
OCF_LE_SET_SCAN_ENABLE = 0x000C
dev_id =0
socket = _BLUETOOTH_.hci_open_dev(dev_id)
##############################################################################
# turn on Bluetooth
def turnOn():
	_OS_.system("rfkill unblock bluetooth")
	print("@bluedev.turnOn(): Turning On Bluetooth.....")
	_TIME_.sleep(0.25)
	_OS_.system("hciconfig -a")
##############################################################################
# turn off Bluetooth
def turnOff():
    _OS_.system("rfkill block bluetooth")
    print("@bluedev.turnOff(): Turning Off Bluetooth.....")
    _TIME_.sleep(0.25)
    _OS_.system("hciconfig -a")
##############################################################################
# check Bluetooth status
def isEnabled():
    result = _SUBPROCESS_.Popen(["hcitool","dev"],stdout=_SUBPROCESS_.PIPE).communicate()[0]
    if "hci" in str(result):
        print("@bluedev.isEnabled(): Bluetooth is Enabled")
        return True
    else:
        print("@bluedev.isEnabled(): Bluetooth is not Enabled")
        return False
##############################################################################
# class for handling scan event
class ScanDelegate(DefaultDelegate):
    def __init__(self):
        self.aqi_flag = True
    def handleDiscovery(self, dev, isNewDev, isNewData):
        list = dev.getScanData()
        isTAG = False
        for x in list:
            if x[0]==3:
                if x[2]=="0000ffff-0000-1000-8000-00805f9b34fb":
                    isTAG=True
            if x[0]==255 and isTAG:
                #print("@bluedev.startSCAN(): creator's TAG raw data :"+x[2])
                bytes = bytearray.fromhex(x[2])
                if len(bytes)==14:
                        data["temperature"] = _STRUCT_.unpack('f', bytes[0:4])[0]
                        data["humidity"] = _STRUCT_.unpack('f', bytes[4:8])[0]
                        data["iaq"] = int.from_bytes(bytes[8:10], byteorder='little', signed=False)
                        data["voc"] = _STRUCT_.unpack('f', bytes[10:14])[0]
                        # get aqi from web
                        if self.aqi_flag:
                            web_data = _REQUESTS_.get(url)
                            json_format = web_data.json()
                            aqi = json_format['data']['aqi']
                        data["aqi"] = aqi
                        data["rssi"] = dev.rssi
                        _MQTT_.publish("sensor",_JSON_.dumps(data))
        _SYS_.stdout.flush()
##############################################################################
# start Bluetooth Scanning
def startSCAN(timeout):
    print("@bluedev.startSCAN(): Enable LE scan")
    scanner= Scanner().withDelegate(ScanDelegate())
    scanner.scan(timeout, passive=True)
    scanner.start()
##############################################################################
# stop Bluetooth Scanning
def stopSCAN(sock=socket):
	print("@bluedev.stopSCAN(): Disable LE scan")
	cmd_pkt = _STRUCT_.pack("<BB", SCAN_DISABLE, 0x00)
	_BLUETOOTH_.hci_send_cmd(sock, OGF_LE_CTL, OCF_LE_SET_SCAN_ENABLE, cmd_pkt)